﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugDraw : MonoBehaviour {

	public class DrawPrimitive
	{
		public float duration;
		public float timeStarted;
		public Color color;

		public virtual void Draw()
		{
			
		}

		public virtual void OnGUI()
		{

		}
	}

	public class DebugSphere : DrawPrimitive
	{
		public Vector3 center;
		public float radius;

		public override void Draw()
		{
			Gizmos.color = color;
			Gizmos.DrawWireSphere(center, radius);
		}
	}

	public class DebugCircle : DrawPrimitive
	{
		public Vector3 center;
		public Vector3 up;
		public Vector3 forward;
		public float radius;

		public override void Draw()
		{
			Gizmos.color = color;
			GizmosDrawCircle(center, up, forward, radius, 12);
		}
	}
	
	public class DebugLine : DrawPrimitive
	{
		public Vector3 begin;
		public Vector3 end;
		
		public override void Draw()
		{
			Gizmos.color = color;
			Gizmos.DrawLine(begin, end);
		}
	}
	
	public class DebugLabel : DrawPrimitive
	{
		public Vector3 location;
		public string text;
		
		public override void Draw()
		{
#if UNITY_EDITOR
			Gizmos.color = color;
			UnityEditor.Handles.BeginGUI();
			GUI.color = color;
			var view = UnityEditor.SceneView.currentDrawingSceneView;
			Vector3 screenPos = view.camera.WorldToScreenPoint(location);
            Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
            GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height + 4, size.x, size.y), text);
			UnityEditor.Handles.EndGUI();
#endif
			Debug.LogFormat("DebugLabel: {0}", text);
		}
	}

	public class DebugLog : DrawPrimitive
	{
		public int side;
		public string text;
		public Color flashColor;
		public float flashFrequency = 0;
	}

	public class DebugHistogram : DrawPrimitive
	{
		public Vector2 position;
		public Vector2 size;
		public int[] histogram;

		public override void OnGUI()
		{
			GUI.color = color;
			int maxCount = histogram.Aggregate(0, (max, item) => Mathf.Max(item, max));
			for (int i = 0; i < histogram.Length; ++i)
			{
				GUI.DrawTexture(new Rect(position + new Vector2(size.x * i / histogram.Length, 0), new Vector2(size.x / histogram.Length, size.y * histogram[i] / maxCount)), WhitePixel);
			}
		}
	}

	public float timeLimit = 0.005f;

	private List<DrawPrimitive> primitives = new List<DrawPrimitive>(200);

	private GUIStyle logStyleLeft = new GUIStyle();
	private GUIStyle logStyleRight = new GUIStyle();
	private Texture2D whitePixel;

	private static DebugDraw instance = null;

	public static Texture2D WhitePixel => Get.whitePixel;

	void Awake()
	{
		DebugDraw.instance = this;

		whitePixel = new Texture2D(1, 1);
		whitePixel.SetPixel(0, 0, Color.white);
		whitePixel.Apply();

		logStyleRight.alignment = TextAnchor.MiddleRight;
		logStyleLeft.alignment = TextAnchor.MiddleLeft;
	}

	static void InitPrimitive(DrawPrimitive p, Color color, float duration)
	{
#if !PRODUCTION_BUILD
		if (DebugDraw.instance == null)
		{
			return;
		}
		
		p.color = color;
		p.duration = duration;
		p.timeStarted = Time.realtimeSinceStartup;
		DebugDraw.instance.primitives.Add(p);
#endif
	}

	public static void Sphere(Vector3 center, float radius, Color color, float duration = 0)
	{
		DebugSphere s = new DebugSphere();
		InitPrimitive(s, color, duration);
		s.center = center;
		s.radius = radius;
	}

	public static void Circle(Vector3 center, Vector3 up, Vector3 forward, float radius, Color color, float duration = 0)
	{
		DebugCircle s = new DebugCircle();
		InitPrimitive(s, color, duration);
		s.center = center;
		s.up = up;
		s.forward = forward;
		s.radius = radius;
	}

	public static void Line(Vector3 begin, Vector3 end, Color color, float duration = 0)
	{
		DebugLine l = new DebugLine();
		InitPrimitive(l, color, duration);
		l.begin = begin;
		l.end = end;
	}
	
	public static void Vector(Vector3 begin, Vector3 direction, Color color, float duration = 0)
	{
		DebugLine l = new DebugLine();
		InitPrimitive(l, color, duration);
		l.begin = begin;
		l.end = begin + direction;

		float a = 0.2f;
		float b = 0.8f;
		
		l = new DebugLine();
		InitPrimitive(l, color, duration);
		l.begin = begin + direction;
		l.end = begin + direction * b - Vector3.Cross(Vector3.up, direction) * a;
		
		l = new DebugLine();
		InitPrimitive(l, color, duration);
		l.begin = begin + direction;
		l.end = begin + direction * b - Vector3.Cross(Vector3.up, -direction) * a;
	}
	
	public static void Label(Vector3 location, string text, Color color, float duration = 0)
	{
		DebugLabel l = new DebugLabel();
		InitPrimitive(l, color, duration);
		l.location = location;
		l.text = text;
	}

	public static void Histogram(int[] histogram, Vector2 position, Vector2 size, Color color, float duration = 0)
	{
		var h = new DebugHistogram();
		InitPrimitive(h, color, duration);
		h.histogram = histogram;
		h.position = position;
		h.size = size;
	}

	public static DebugLog Log(string text, Color color, int side = 0, float duration = 0)
	{
		var log = new DebugLog();
		InitPrimitive(log, color, duration);
		log.flashColor = color;
		log.side = side;
		log.text = text;

		return log;
	}

	public static void LogOk(string text, int side = 0, float duration = 5)
	{
		Log(text, Color.green, side, duration);
		Debug.Log(text);
	}
	
	public static void LogOkFormat(string text, int side = 0, float duration = 5, params object[] args)
	{
		var fmtText = string.Format(text, args);
		Log(fmtText, Color.green, side, duration);
		Debug.Log(fmtText);
	}
	
	public static void LogErrorFormat(string text, int side = 0, float duration = 30, params object[] args)
	{
		var fmtText = string.Format(text, args);
		Log(fmtText, Color.red, side, duration);
		Debug.LogError(fmtText);
	}
	
	public static void LogOkTime(string text, int side = 0, float duration = 5)
	{
		Log(Time.realtimeSinceStartup.ToString("0.00") + ":: " + text, Color.green, side, duration);
		Debug.Log(text);
	}
	
	public static void LogWarningFormat(string text, int side = 0, float duration = 5, params object[] args)
	{
		var fmtText = string.Format(text, args);
		Log(fmtText, Color.yellow, side, duration);
		Debug.LogWarning(fmtText);
	}

	public static void LogWarning(string text, int side = 0, float duration = 5)
	{
		Log(text, Color.yellow, side, duration);
		Debug.LogWarning(text);
	}

	public static void LogError(string text, int side = 0, float duration = 5)
	{
		Log(text, Color.red, side, duration);
		Debug.LogError(text);
	}

	public static void LogFlash(string text, Color flashColor = default(Color), float flashFrequency = 6, int side = 0, float duration = 5)
	{
		var log = Log(text, Color.yellow, side, duration);
		if (flashColor == default(Color))
		{
			log.flashColor = Color.magenta;
		}
		log.flashFrequency = flashFrequency;

		Debug.Log(text);
	}

	private void OnDrawGizmos()
	{
		var startTime = Time.realtimeSinceStartup;
		
		for (int i = 0; i < primitives.Count; ++i)
		{
			primitives[i].Draw();
			if (i % 10 == 0)
			{
				if (Time.realtimeSinceStartup - startTime > timeLimit)
				{
					break;
				}
			}
		}

		primitives.RemoveAll(s => Time.realtimeSinceStartup > s.timeStarted + s.duration);
	}

	private void AddLogLine(DebugLog log, float lineHeight, float xOffset, ref float yOffset, GUIStyle style)
	{
		if (yOffset > Screen.height)
		{
			return;
		}
		style.normal.textColor = Color.black;
		GUI.Label(new Rect(xOffset + 1, yOffset + 1, Screen.width - xOffset * 2, lineHeight), log.text, style);
		if (log.flashFrequency != 0)
		{
			style.normal.textColor = Color.Lerp(log.color.linear, log.flashColor.linear, Mathf.Sin(Time.realtimeSinceStartup * log.flashFrequency) * 0.5f + 0.5f).gamma;
		}
		else
		{
			style.normal.textColor = log.color;
		}
		GUI.Label(new Rect(xOffset, yOffset, Screen.width - xOffset * 2, lineHeight), log.text, style);

		yOffset += lineHeight;
	}

	private void OnGUI()
	{
		primitives.RemoveAll(s => Time.realtimeSinceStartup > s.timeStarted + s.duration);

		float xOffset = 10;
		float yOffsetLeft = 0;
		float yOffsetRight = 0;
		float lineHeight = 20;
		for (int i = primitives.Count - 1; i >= 0; --i)
		{
			var log = primitives[i] as DebugLog;
			if (log != null)
			{
				if (log.side == 0)
				{
					AddLogLine(log, lineHeight, xOffset, ref yOffsetLeft, logStyleLeft);
				}
				else if (log.side == 1)
				{
					AddLogLine(log, lineHeight, xOffset, ref yOffsetRight, logStyleRight);
				}
			}
		}

		for (int i = 0; i < primitives.Count; ++i)
		{
			primitives[i].OnGUI();
		}
	}


	public static void GizmoArrowLine(Vector3 from, Vector3 to, float wingLength = 0.1f, float wingSpacing = 0.2f)
	{
		Gizmos.DrawLine(from, to);
		float len = Vector3.Distance(from, to);
		Vector3 dir = (to - from).normalized;
		Vector3 wing = (Camera.current.transform.right- dir).normalized;
		for (float d = 0; d < len; d += wingSpacing)
		{
			Gizmos.DrawLine(from + dir * d, from + dir * (d - wingLength) + wing * wingLength);
			Gizmos.DrawLine(from + dir * d, from + dir * (d - wingLength) - wing * wingLength);
		}
	}

	public static void GizmosDrawCircleArc(Vector3 center, Vector3 normal, Vector3 forward, float radius, float minAngle, float maxAngle)
	{
		Vector3 v = forward * radius;

		Gizmos.DrawLine(center, center + Quaternion.AngleAxis(minAngle, normal) * v);
		Gizmos.DrawLine(center, center + Quaternion.AngleAxis(maxAngle, normal) * v);

		float a = minAngle + 30;
		for (; a < maxAngle; a += 30)
		{
			Gizmos.DrawLine(center + Quaternion.AngleAxis(a - 30, normal) * v, center + Quaternion.AngleAxis(a, normal) * v);
		}

		Gizmos.DrawLine(center + Quaternion.AngleAxis(a - 30, normal) * v, center + Quaternion.AngleAxis(maxAngle, normal) * v);
	}

	public static void GizmosDrawCircle(Vector3 center, Vector3 normal, Vector3 forward, float radius, int edges = 12)
	{
		Vector3 v = forward * radius;

		for (float a = 0; a <= 360; a += 360.0f / edges)
		{
			Gizmos.DrawLine(center + Quaternion.AngleAxis(a, normal) * v, center + Quaternion.AngleAxis(Mathf.Min(360, a + 360.0f / edges), normal) * v);
		}
	}
	
	public static void GizmosDrawCircleSlices(Vector3 center, Vector3 normal, Vector3 forward, float radius, int edges = 12)
	{
		Vector3 v = forward * radius;

		for (float a = 360 / edges; a <= 360; a += 360 / edges)
		{
			Gizmos.DrawLine(center, center + Quaternion.AngleAxis(a, normal) * v);
		}
	}

	public static DebugDraw Get 
	{
		get
		{
			if (!instance)
			{
				instance = FindObjectOfType<DebugDraw>();
				if (instance == null)
				{
					Debug.LogError("Missing debug draw.");
				}
			}
			
			return instance;
		}
	}
}

﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
	struct FindResult<U>
	{
		public U item;
		public bool notFound;
	}

	private static T _instance = null;
	private static bool isInitialized = false;
	
	public static T Instance 
	{
		get
		{
			if (!isInitialized)
			{
				var result = _FindInAllScenes();
				if (result.notFound)
				{
					Debug.LogError("Missing single instance of " + typeof(T).Name + " on the scene.");
					return null;
				}
				else
				{
					_instance = result.item;
				}
                isInitialized = true;
			}

			return _instance;
		}
	}

	public static T Get => Instance;

	static FindResult<T> _FindInAllScenes()
	{
		bool notFound = true;
		for (int i = 0; i < SceneManager.sceneCount; ++i)
		{
			var t = _FindInScene(SceneManager.GetSceneAt(i));
			if (t.item != null)
			{
				return t;
			}

			notFound = notFound && t.notFound;
		}

		return new FindResult<T>() { notFound = notFound };
	}

	static FindResult<T> _FindInScene(Scene scene)
	{
		if (!scene.isLoaded)
		{
			return new FindResult<T>() { notFound = false };
		}

		foreach (var root in scene.GetRootGameObjects())
		{
			var t = _FindInHierarchy(root.transform);
			if (t != null)
			{
				return new FindResult<T>() { item = t };
			}
		}
		return new FindResult<T>() { notFound = true };
	}

	static T _FindInHierarchy(Transform transform)
	{
		var t = transform.GetComponent<T>();
		if (t != null)
		{
			return t;
		}
		for (int i = 0; i < transform.childCount; ++i)
		{
			t = _FindInHierarchy(transform.GetChild(i));
			if (t != null)
			{
				return t;
			}
		}
		return null;
	}

	public static bool HasInstance => isInitialized;

	protected virtual void OnDestroy()
	{
		Debug.Log("Destroying MonoBehvaiour Singleton " + GetType().Name);
		
		isInitialized = false;
		_instance = null;
	}
}


﻿using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Reflection;

[System.AttributeUsage(System.AttributeTargets.Field)]
public class InspectorBitsAttribute : PropertyAttribute
{
	public static float kDefaultWidth = 450;
	public string[] labels = new string[]{};

	private float _labelWidth = kDefaultWidth;
	public float LabelWidth 
	{
		get { return _labelWidth; }
		set { _labelWidth = value; }
	}

	public InspectorBitsAttribute(string[] labels)
	{
		this.labels = labels;
		if (this.labels == null)
		{
			this.labels = new[] {"Bit 0", "Bit 1", "Bit 2", "Bit 3", "Bit 4", "Bit 5", "Bit 6", "bit 7"};
		}
	}
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(InspectorBitsAttribute))]
public class InspectorBitsPropertyDrawer : PropertyDrawer
{
	float height = 20;

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		var attr = (InspectorBitsAttribute)attribute;
		return height * (attr.labels.Length + 1);
	}

	public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
	{
		var attr = (InspectorBitsAttribute)attribute;
		int bits = prop.intValue;

		EditorGUI.LabelField(new Rect(position.x, position.y, attr.LabelWidth, height), prop.name);

		for (int i = 0; i < attr.labels.Length; ++i)
		{
            var labelRect = new Rect(position.x+20, position.y + (i + 1) * height, attr.LabelWidth-20, height);
            bits = bits & (~(1 << i)) | ((EditorGUI.Toggle(labelRect, attr.labels[i], (bits & (1 << i)) != 0) ? 1 : 0) << i);
		}

		prop.intValue = bits;

		EditorUtility.SetDirty(prop.serializedObject.targetObject);

	}
}
#endif

﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace BattleRush
{
    public class Hud : MonoBehaviourSingleton<Hud>
    {
        public GameObject deckContainer;
        public GameObject APContainer;
        public GameObject AP;

        public Card performedCard;
        

        public async Task PerformHudInput(Player player)
        {
            deckContainer.DestroyAllChildren();
            
            foreach (var card in player.Cards)
            {
                var hudCard = card.hudCardPrefab.PoolInstantiate();
                hudCard.transform.SetParent(deckContainer.transform, false);
                hudCard.GetComponent<HudCard>().Init(card, OnCardPerformClick);
            }

            performedCard = null;

            while (performedCard == null)
            {
	            await new UnityAsync.WaitForFrames(1);
            }
            
            deckContainer.DestroyAllChildren();
        }

        void OnCardPerformClick(Card cardo)
        {
            performedCard = cardo;
        }
        // public async Task ShowAP(Critter critter){
        //     var GM = FindObjectOfType<GameManager>();
        //     while(GM.isPlayerTurn){
        //         }
        //     }
        // }

        // GameObject[] APs;
        List<GameObject> APs = new List<GameObject>();

        public void ShowAP(Critter critter){
            for (int i = 0; i < critter.AP; i++)
            {
                APs.Add(Pool.Get.Take(AP));
                print("dups");

                APs[i].transform.SetParent(APContainer.transform);               
            }
            for (int i = 0; i < critter.currentAP; i++){
                APs[i].transform.GetChild(0).gameObject.SetActive(true);
            }
        }
        public void ClearAP(){
            foreach (var body in APs)
            {
                body.transform.GetChild(0).gameObject.SetActive(false);
                Pool.Get.Release(body);
            }
        }
    }

}

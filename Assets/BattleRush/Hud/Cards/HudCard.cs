﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BattleRush
{
    public class HudCard : MonoBehaviour
    {
        public GameObject pivot;
        public Button drawButton;
        public Button performButton;

        [HideInInspector]
        public Card card;

        bool isDrawn;

        Action<Card> onPerform;
        
        public void Init(Card card, Action<Card> onPerform)
        {
            this.card = card;
            this.onPerform = onPerform;
            
            drawButton.onClick.AddListener(OnDrawClick);
            performButton.onClick.AddListener(OnPerformClick);
        }

        void OnPerformClick()
        {
            if (!isDrawn)
            {
                OnDrawClick();
                return;
            }

            onPerform?.Invoke(card);
        }

        void OnDrawClick()
        {
            if (!isDrawn)
            {
                pivot.transform.localPosition = new Vector3(0, 30, 0);
                isDrawn = true;
            }
            else
            {
                pivot.transform.localPosition = Vector3.zero;
                isDrawn = false;
            }
        }


        // Update is called once per frame
        void Update()
        {
            
        }
    }

}


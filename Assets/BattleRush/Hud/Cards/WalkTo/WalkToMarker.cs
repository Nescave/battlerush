﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BattleRush
{
    public class WalkToMarker : MonoBehaviour
    {
        public int step;

        public Event<WalkToMarker> clicked = new Event<WalkToMarker>();

        void Taken()
        {
            clicked.RemoveAllListeners();
        }
        
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
        }

        void OnMouseEnter()
        {
            transform.localScale = Vector3.one * 0.9f;
        }

        void OnMouseExit()
        {
            transform.localScale = Vector3.one;
        }

        void OnMouseOver()
        {
            if (Input.GetMouseButtonDown(0))
            {
                clicked?.Invoke(this);
            }
            
        }
    }

}

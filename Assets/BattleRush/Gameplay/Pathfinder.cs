﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace BattleRush
{
        
    public class Pathfinder : MonoBehaviourSingleton<Pathfinder>
    {
        public delegate float CostFunc(Tile a, Tile b);
        
        public class ManhattanRange
        {
            public List<Tile> startTiles = new List<Tile>();
            public int range;
            public Dictionary<Tile, float> tileCost = new Dictionary<Tile, float>();
            public Dictionary<Tile, Tile> previous = new Dictionary<Tile, Tile>();
            public CostFunc costFunc;

            public IEnumerable<Tile> Tiles => tileCost.Keys;
        }
        
        public ManhattanRange GetTilesInManhattanRange(Tile startTile, int range, CostFunc costFunc)
        {
            var result = new ManhattanRange()
            {
                range = range,
                costFunc = costFunc
            };
            
            result.startTiles.Add(startTile);
            
            Queue<Tile> queue = new Queue<Tile>();
            queue.Enqueue(startTile);
            
            result.tileCost[startTile] = 0;
            result.previous[startTile] = null;
            
            while (queue.Count > 0)
            {
                var t = queue.Dequeue();

                foreach (var nb in t.ValidNeighbors)
                {
                    if (
                        !result.tileCost.ContainsKey(nb) && 
                        nb.IsWalkable &&
                        result.tileCost[t] + 1 < range &&
                        !nb.IsOccupied
                        )
                    {
                        result.tileCost[nb] = result.tileCost[t] + costFunc(t, nb);
                        result.previous[nb] = t;
                        queue.Enqueue(nb);
                    }
                }
            }

            return result;
        }

        public class ManhattanPath
        {
            public ManhattanRange range;
            public List<Tile> path;
        }

        public ManhattanPath FindPath(ManhattanRange range, Tile startTile, Tile endTile)
        {
            var path = new List<Tile>();

            var pointerTile = endTile;

            if(!range.previous.ContainsKey(pointerTile)){
                return null;
            }
            
            while (pointerTile != startTile)
            {
                path.Add(pointerTile);
                pointerTile = range.previous[pointerTile];
            }
            path.Reverse();

            return new ManhattanPath()
            {
                path = path,
                range = range
            };
        }

        public ManhattanPath FindPath(Tile startTile, Tile endTile, int range, CostFunc costFunc)
        {
            var rangeTiles = GetTilesInManhattanRange(startTile, range, costFunc);

            return FindPath(rangeTiles, startTile, endTile);
        }
    }

}

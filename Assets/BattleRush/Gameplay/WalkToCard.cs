﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace BattleRush
{
    public class WalkToCard : Card
    {
        public GameObject walkToMarkerPrefab;
        
        public virtual int Range => 5;

        List<WalkToMarker> hudMarkers = new List<WalkToMarker>();

        WalkToMarker clickedMarker;
        
        public override async Task Perform(Player player)
        {
            var startTile = player.controlledCritter.GetTile();

            var rangeTiles = Pathfinder.Get.GetTilesInManhattanRange(startTile, Range, (a,b)=>1);

            foreach (var t in rangeTiles.Tiles)
            {
                var marker = Pool.Get.Take(walkToMarkerPrefab);
                marker.transform.position = t.WorldLocation;
                var walkToMarker = marker.GetComponent<WalkToMarker>();
                walkToMarker.step = Mathf.RoundToInt(rangeTiles.tileCost[t]);
                hudMarkers.Add(walkToMarker);
            }

            foreach (var marker in hudMarkers)
            {
                marker.clicked.AddListener(OnMarkerClicked);
            }

            clickedMarker = null;

            await Async.WaitUntil(() => clickedMarker != null);

            var path = Pathfinder.Get.FindPath(rangeTiles, startTile, clickedMarker.GetTile());
            
            ClearMarkers();

            foreach (var pathTile in path.path)
            {
	            await player.controlledCritter.Walk(pathTile);
            }
            
        }

        void OnMarkerClicked(WalkToMarker obj)
        {
            clickedMarker = obj;
        }

        public void ClearMarkers()
        {
            foreach (var marker in hudMarkers)
            {
                Pool.Get.Release(marker.gameObject);
            }
            hudMarkers.Clear();
        }

        void Update()
        {
        }
    }

}


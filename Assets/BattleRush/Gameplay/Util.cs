﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BattleRush
{
    public class Event<T> : UnityEvent<T>
    {
    }
    
    public static class Util 
    {
        public static T PickRandom<T>(this IEnumerable<T> collection)
        {
            T choosenItem = default(T);

            var k = 1;

            foreach (var item in collection)
            {
                if (Random.value < 1.0f / k)
                {
                    choosenItem = item;
                }

                k++;
            }

            return choosenItem;
        }

        public static void DestroyAllChildren(this GameObject go)
        {
            var items = new List<Transform>();
            for (int i = 0; i < go.transform.childCount; ++i)
            {
                items.Add(go.transform.GetChild(i));
            }

            foreach (var child in items)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        
        public static GameObject PoolInstantiate(this GameObject prefab)
        {
            return GameObject.Instantiate(prefab);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BattleRush
{
    public class Pool : MonoBehaviourSingleton<Pool>
    {
        private Dictionary<GameObject, GameObject> prefabByInstance = new Dictionary<GameObject, GameObject>();
        private Dictionary<GameObject, List<GameObject>> freeInstancesByPrefab = new Dictionary<GameObject, List<GameObject>>();
            
        public GameObject Take(GameObject prefab)
        {
            List<GameObject> instances = null;
            GameObject instance = null;
            if (freeInstancesByPrefab.TryGetValue(prefab, out instances))
            {
                if (instances.Count > 0)
                {
                    instance = instances[instances.Count - 1];
                    instances.RemoveAt(instances.Count - 1);
                }
            }

            if (instance == null)
            {
                instance = Instantiate(prefab);
                prefabByInstance[instance] = prefab;
            }

            InitTakenInstance(instance, prefab);
                    
            return instance;
        }

        void InitTakenInstance(GameObject instance, GameObject prefab)
        {
            instance.transform.localPosition = prefab.transform.localPosition;
            instance.transform.localRotation = prefab.transform.localRotation;
            instance.transform.localScale = prefab.transform.localScale;
            instance.SetActive(true);
            instance.SendMessage("Taken", SendMessageOptions.DontRequireReceiver);
        }
        
        public GameObject Take(GameObject prefab, Transform parent)
        {
            List<GameObject> instances = null;
            GameObject instance = null;
            if (freeInstancesByPrefab.TryGetValue(prefab, out instances))
            {
                if (instances.Count > 0)
                {
                    instance = instances[instances.Count - 1];
                    instances.RemoveAt(instances.Count - 1);
                }
            }

            if (instance == null)
            {
                instance = Instantiate(prefab, parent);
                prefabByInstance[instance] = prefab;
            }
            
            instance.transform.SetParent(parent, false);
            InitTakenInstance(instance, prefab);
            
            return instance;
        }

        public void Release(GameObject instance)
        {
            GameObject prefab = null;
            if (!prefabByInstance.TryGetValue(instance, out prefab))
            {
                Debug.LogError("GameObject was not created by pool.", instance);
                return;
            }
            
            List<GameObject> instances = null;
            if (!freeInstancesByPrefab.TryGetValue(prefab, out instances))
            {
                freeInstancesByPrefab[prefab] = instances = new List<GameObject>();
            }
            
            instances.Add(instance);
            
            instance.transform.SetParent(transform, false);
            instance.SetActive(false);
        }

        public void ReleaseDirectChildren(GameObject parent)
        {
            var itemsToRelease = new List<GameObject>();
            for (int i = 0; i < parent.transform.childCount; ++i)
            {
                var t = parent.transform.GetChild(i);
                if (prefabByInstance.ContainsKey(t.gameObject))
                {
                    itemsToRelease.Add(t.gameObject);
                }
            }

            foreach (var item in itemsToRelease)
            {
                Release(item);
            }
            
        }

        public void ReleaseAllChildren(GameObject parent)
        {
            for (int i = 0; i < parent.transform.childCount; ++i)
            {
                ReleaseAllChildren(parent.transform.GetChild(i).gameObject);
            }
            
            ReleaseDirectChildren(parent);
        }
    }

}

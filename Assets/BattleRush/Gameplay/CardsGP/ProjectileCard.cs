﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;  

namespace BattleRush{

    public class ProjectileCard : Card
    {
        public GameObject walkToMarkerPrefab;
        public GameObject projectilePrefab;
        public bool reached = false; 
        WalkToMarker clickedMarker;
        public int Range => 4;
        public float force = 2f;
        List<WalkToMarker> hudMarkers = new List<WalkToMarker>();

        public override async Task Perform(Player player){
            
            reached = false;
            var startTile = player.controlledCritter.GetTile();
            var cornerTileLoc = new Tiloc(startTile.Loc.x - Range, startTile.Loc.y - Range);
            var cornerTile = GameManager.Get.GetTile(new Vector3(startTile.Loc.x-Range, 0, startTile.Loc.y - Range));

            for (int x = 0; x <= Range*2; x++){
                var currentRowLoc = new Tiloc(cornerTileLoc.x + x, cornerTileLoc.y);
                for (int y = 0; y <= Range*2;y++){
                    var currentTile = GameManager.Get.GetTile(new Vector3(currentRowLoc.x, 0f, currentRowLoc.y+y));
                    var distance = Vector3.Distance(startTile.WorldLocation, currentTile.WorldLocation);

                    if(distance <= Range && currentTile.IsWalkable){
                        var marker = Pool.Get.Take(walkToMarkerPrefab);
                        marker.transform.position = currentTile.WorldLocation;
                        var WalkToMarker = marker.GetComponent<WalkToMarker>();
                        hudMarkers.Add(WalkToMarker);
                    }
                }
            }

            foreach (var marker in hudMarkers){
                marker.clicked.AddListener(OnMarkerClicked);
            }

            clickedMarker = null;
            await Async.WaitUntil(() => clickedMarker != null);
            var TargetTile = clickedMarker.GetTile();
            var Direction = (TargetTile.WorldLocation - startTile.WorldLocation).normalized;
            ClearMarkers();
            var projectile = SpawnProjectile(Direction, startTile.WorldLocation);

            await Async.WaitUntil(() => projectileReached(projectile, TargetTile) );
            await Async.WaitForSeconds(.1f);

            projectile.GetComponent<Rigidbody>().velocity = Vector3.zero;
            Pool.Get.Release(projectile);


            // await Async.WaitUntil(() => clickedMarker != null);
        }

        void OnMarkerClicked(WalkToMarker obj)
        {
            clickedMarker = obj;
        }
        void ClearMarkers(){
            foreach(var marker in hudMarkers){
                Pool.Get.Release(marker.gameObject);
            }
            hudMarkers.Clear();
        }
        GameObject SpawnProjectile(Vector3 direction, Vector3 startPos){
            var projectile = Pool.Get.Take(projectilePrefab);
            projectile.transform.position = startPos+Vector3.up;
            projectile.transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
            projectile.GetComponent<Projectile>().parentCard = this;
            var rb = projectile.GetComponent<Rigidbody>();
            rb.AddForce(direction * force, ForceMode.Impulse);
            return projectile;
        }
        bool projectileReached(GameObject proj, Tile targetTile){
            
            var dist = (targetTile.WorldLocation - proj.transform.position).magnitude; 
            if( GameManager.Get.GetTile(proj.transform.position) == targetTile ){
                reached = true;
            }
            return reached;
        }
    }
}
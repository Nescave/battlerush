﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BattleRush{
    public class Projectile : MonoBehaviour
    {

        public ProjectileCard parentCard;
        public int Damage =2;
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }
        void OnTriggerEnter(Collider col){
            if(col.gameObject.layer == 8){
                col.GetComponentInParent<Critter>().DealDamage(Damage);
            }else if(col.gameObject.layer == 9){
                Pool.Get.Release(gameObject);
                parentCard.reached = true;
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace BattleRush
{

    public class Player : MonoBehaviour
    {
        public Critter controlledCritter;

        public IEnumerable<Card> Cards
        {
            get
            {
                foreach (var card in GetComponentsInChildren<Card>())
                {
                    yield return card;
                }
            }
        }
        
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }


        public async Task PerformPlayerTurn()
        {

            controlledCritter.RestoreAP();
            Hud.Get.ShowAP(controlledCritter);
            print("dups2");

	        await Hud.Get.PerformHudInput(this);
            
            if (Hud.Get.performedCard != null)
            {
                if (controlledCritter.UseAP( Hud.Get.performedCard.APCost)){
	                await Hud.Get.performedCard.Perform(this);
                    Hud.Get.ClearAP();
                }
            }
            else
            {
	            await Task.Delay(1);
            }
        }
    }
       

}

﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace BattleRush
{
    public class Critter : MonoBehaviour
    {
        public int HP;
        public GameObject HPBarPrefab;
        private HP_Bar HP_Bar_Instance;

        public int AP = 1;
        public int APRegeneration = 1;
        public int currentAP;


        void Start(){
            GameManager.Get.GetTile(transform.position).IsOccupied = true;
            var HPBar = Pool.Get.Take(HPBarPrefab);
            var critterMesh = transform.GetComponentInChildren<MeshFilter>();
            HPBar.transform.position = transform.position + new Vector3(0,critterMesh.mesh.bounds.extents.y,0);
            HPBar.transform.SetParent(transform, true);
            HP_Bar_Instance = HPBar.GetComponent<HP_Bar>(); 
            HP_Bar_Instance.maxHP = HP;
            HP_Bar_Instance.setHP(HP);
            currentAP = AP;
        }
        public async Task Walk(Tile targetTile)
        {
            var startLoc = transform.position;
            var endLoc = targetTile.WorldLocation;
            
			await Async.TweenWith(0.2f, t =>
            {
                GameManager.Get.GetTile(startLoc).IsOccupied = false;
                transform.position = Vector3.Lerp(startLoc, endLoc, Mathf.SmoothStep(0, 1,t));
                targetTile.IsOccupied = true;
            });
        }
        public void Die (){
            gameObject.SetActive(false);
            GameManager.Get.GetTile(transform.position).IsOccupied = false;
        }
        public void DealDamage(int damage){
            HP -= damage;
            if(HP <= 0){
                Die();
            }else{
                HP_Bar_Instance.setHP(HP);
            }
        }
        public bool UseAP(int ammount){
            if(currentAP >= ammount){
                currentAP -= ammount;
                return true;
            } else{
                return false;
            }
        }
        public void RestoreAP(){
            currentAP += APRegeneration;
        }
    }

}


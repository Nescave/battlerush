﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityAsync;
using UnityEngine;

namespace BattleRush
{
    public static class TileUtils
    {
        public static Tiloc ToTiloc(this Vector3 v3)
        {
            return new Tiloc(v3);
        }

        public static Tiloc GetTiloc(this Component comp)
        {
            return comp.transform.position.ToTiloc();
        }
        
        public static Tiloc GetTiloc(this GameObject go)
        {
            return go.transform.position.ToTiloc();
        }

        public static Tile GetTile(this Component comp)
        {
            return GameManager.Get.GetTile(comp.GetTiloc());
        }
        
        public static Tile GetTile(this GameObject go)
        {
            return GameManager.Get.GetTile(go.GetTiloc());
        }
    }

    public struct Tiloc
    {
        public int x;
        public int y;

        public Tiloc(int x = 0, int y = 0)
        {
            this.x = x;
            this.y = y;
        }
        
        public Tiloc(Vector3 v3)
        {
            this.x = Mathf.RoundToInt(v3.x);
            this.y = Mathf.RoundToInt(v3.z);
        }

        public Vector3 ToVector3()
        {
            return new Vector3(x, 0, y);
        }
        
        public IEnumerable<Tiloc> Neighbors
        {
            get
            {
                yield return new Tiloc(x + 1, y);
                yield return new Tiloc(x - 1, y);
                yield return new Tiloc(x, y + 1);
                yield return new Tiloc(x, y - 1);
            }
        }

        public override string ToString()
        {
            return $"[{x},{y}]";
        }
    }

    public class Tile
    {
        public Tile(Tiloc loc)
        {
            Loc = loc;
        }
        
        public Tiloc Loc { get; private set; }
        
        public List<SceneTile> sceneTiles = new List<SceneTile>();

        public Vector3 WorldLocation => Loc.ToVector3();

        public bool IsWalkable => sceneTiles.All(st => st.isWalkable);
        public bool IsOccupied = false;

        public IEnumerable<Tile> ValidNeighbors
        {
            get
            {
                foreach (var nbLoc in Loc.Neighbors)
                {
                    var nbTile = GameManager.Get.GetTile(nbLoc);
                    if (nbTile != null)
                    {
                        yield return nbTile;
                    }
                }
            }
        }
    }

    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        Dictionary<Tiloc, Tile> tiles = new Dictionary<Tiloc, Tile>();
        
        //TODO proper initialization
        public Player player;

        public Player CurrentPlayer => player;

        public IEnumerable<Player> Players
        {
            get { yield return player; }
        }

        public IEnumerable<Ai> Ais
        {
            get
            {
                foreach (var ai in FindObjectsOfType<Ai>())
                {
                    yield return ai;
                }
            }
        } 
        
        void Start()
        {
            InitGrid();

            StartCoroutine(ProcessTurn().AsYieldInstruction());
        }
        
        public Tile GetTile(Vector3 v)
        {
            return GetTile(v.ToTiloc());
        }

        public Tile GetTile(Tiloc tl)
        {
            if (!tiles.ContainsKey(tl))
            {
                tiles[tl] = new Tile(tl);
            }

            return tiles[tl];
        }

        public void InitGrid()
        {
            var grid = FindObjectOfType<Grid>();
            if (grid == null)
            {
                Debug.LogError("Missing grid");
                return;
            }

            var allSceneTiles = grid.GetComponentsInChildren<SceneTile>();
            foreach (var st in allSceneTiles)
            {
                GetTile(st.transform.position).sceneTiles.Add(st);
            }
            
            Debug.Log($"Grid initialized with {tiles.Count} tiles");
        }
        
        public bool isPlayerTurn;

        async Task ProcessTurn()
        {
            while (true)
            {
                foreach (var p in Players)
                {
                    isPlayerTurn = true;
	                await p.PerformPlayerTurn();

                    isPlayerTurn = false;

                    foreach (var ai in Ais)
                    {
						await ai.Perform();
                    }
                }
            }
        }
    }

}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace BattleRush
{
    public static class Async
    {
	    public static async Task TweenWith(float time, Action<float> func)
	    {
            var startTime = Time.time;
            var endTime = startTime + time;
            do
            {
	            var t01 = Mathf.Clamp01((Time.time - startTime) / (endTime - startTime));
	            func?.Invoke(t01);
	            await new UnityAsync.WaitForFrames(1);
            } while (Time.time < endTime);
	    }

	    public static async Task WaitUntil(Func<bool> condition)
	    {
		    while (!condition())
		    {
			    await new UnityAsync.WaitForFrames(1);
		    }
	    }
		public static async Task WaitForSeconds(float seconds){
			float currentTime = 0f;
			while(currentTime<seconds){
				currentTime += Time.deltaTime;
				await new UnityAsync.WaitForFrames(1);
			}			
		}
    }
}


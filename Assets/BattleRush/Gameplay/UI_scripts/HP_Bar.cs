﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BattleRush{
    public class HP_Bar : MonoBehaviour
    {

        public float maxHP;
        public Transform HPBar;

        void Awake() {
            if(transform.GetChild(0) != null){
                HPBar = transform.GetChild(0);
            }
        }
        public void setHP(int currentHP){
            
            var HPBarScale = currentHP/maxHP;

            if(HPBar != null) { 

                HPBar.localScale = new Vector3(HPBarScale, 1,1);
            }
        }
    }
}
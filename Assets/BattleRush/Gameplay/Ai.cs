﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace BattleRush
{
    public class Ai : MonoBehaviour
    {
        Critter critter;
        private GameObject player;
        public int walkRange;
        public virtual int hearRange => 6;
        public int a;
        
        void Awake()
        {
            player = GameObject.Find("Jesus");
            critter = GetComponent<Critter>();
        }

        public async Task Perform()
        {
            // var targetTile = player.GetTile();
            var startTile = gameObject.GetTile();
            DebugDraw.Sphere(gameObject.transform.position,.5f, Color.red, 2f);
            var tilesInRange = Pathfinder.Get.GetTilesInManhattanRange(startTile, hearRange, (a,b) => 1 );
            Tile targetTile = null;
            foreach(var tile in player.GetTile().ValidNeighbors){
                if(tilesInRange.tileCost.ContainsKey(tile)){
                    if (targetTile == null) targetTile = tile;
                    else if (tilesInRange.tileCost[targetTile] > tilesInRange.tileCost[tile]){
                        targetTile = tile;
                    }
                }
            }
            if (targetTile == null || targetTile == startTile) return;
            var path = Pathfinder.Get.FindPath(tilesInRange, startTile, targetTile);
            var range = walkRange;
            if (path.path.Count < walkRange){
                range = path.path.Count;
            } 
            for (int i = 0; i < range; i++){
                // DebugDraw.Sphere(gameObject.transform.position,.5f, Color.red, 5f);
                // DebugDraw.Sphere(path.path[i].WorldLocation,.5f, Color.blue, 5f);
                if(!path.path[i].IsOccupied){
                    await critter.Walk(path.path[i]);    
                }else return;
            }
        }
    }
}


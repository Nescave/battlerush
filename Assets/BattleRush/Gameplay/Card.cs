﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace BattleRush
{
    public class Card : MonoBehaviour
    {
        public GameObject hudCardPrefab;
        public int APCost = 1;
        
        public virtual async Task Perform(Player player)
        {
	        return;
        }
        
    }

}

